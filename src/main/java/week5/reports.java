package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class reports {
	
public static void main(String[] args) throws IOException  {
	ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
    html.setAppendExisting(true);
    ExtentReports extent=new ExtentReports();
    extent.attachReporter(html);
    //TEST CASE LEVEL
    ExtentTest test=extent.createTest("TC001_CreateTestlead", "Create tset lead");
    test.assignCategory("smoke");
    test.assignAuthor("Kalpana");
    test.pass("browser is launched succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
    test.pass("demoslaesmanager is enteed suceesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
    test.fail("crmsfa is not enteed suceesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
    extent.flush();
	}
}
