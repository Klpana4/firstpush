package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	public static Object[][] getexceldata(String excelfilename) throws IOException {
		// TODO Auto-generated method stub
		
		XSSFWorkbook wbook=new XSSFWorkbook("./data/"+excelfilename+".xlsx");
		
		XSSFSheet sheetAt = wbook.getSheetAt(0);
		int lastRowNum = sheetAt.getLastRowNum();
		System.out.println(lastRowNum);
		
        int lastCellNum = sheetAt.getRow(0).getLastCellNum();
        System.out.println(lastCellNum);
        Object[][] data=new Object[lastRowNum][lastCellNum];
        for(int i=1;i<=lastRowNum;i++)
        {
        XSSFRow row = sheetAt.getRow(i);
        
        for (int j=0;j<lastCellNum;j++)
        {
        XSSFCell cell = row.getCell(j);
        String stringCellValue = cell.getStringCellValue();
        data[i-1][j]=cell.getStringCellValue();
       	System.out.println(stringCellValue);
        	
         }
        
        

	}
		return data;

}
}
