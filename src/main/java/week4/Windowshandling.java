package week4;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Windowshandling {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver(); 
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
         Set<String> windowHandles = driver.getWindowHandles();
         List<String> allwin=new ArrayList<>();
         allwin.addAll(windowHandles);
         
        driver.switchTo().window(allwin.get(1));
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        
        File screenshotAs = driver.getScreenshotAs(OutputType.FILE);
        File src=new File("./snaps/kalpana.png");
        FileUtils.copyFile(screenshotAs, src);
        driver.switchTo().window(allwin.get(0)).close();
        
        
     
     
     
     
	}

}
