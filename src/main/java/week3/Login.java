package week3;

import java.sql.Driver;
import java.util.*;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args)  {
		
			// TODO Auto-generated method stub
			
System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
ChromeDriver driver = new ChromeDriver(); 
driver.get("http://leaftaps.com/opentaps");
driver.manage().window().maximize();
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

try {
driver.findElementById("username").sendKeys("DemoSalesManager");
driver.findElementById("password").sendKeys("crmsfa");
driver.findElementByClassName("decorativeSubmit").click();

driver.findElementByLinkText("CRM/SFA").click();
driver.findElementByLinkText("Create Lead").click();
driver.findElementById("createLeadForm_companyName").sendKeys("cognizant");
//Thread.sleep(1000);
driver.findElementById("createLeadForm_firstName").sendKeys("kalpana");
driver.findElementById("createLeadForm_lastName").sendKeys("kuntal");

WebElement src = driver.findElementById("createLeadForm_dataSourceId");

WebElement src1=driver.findElementById("createLeadForm_marketingCampaignId");
Select obj = new Select(src);
obj.selectByVisibleText("Direct Mail");

Select obj1 = new Select(src1);
List<WebElement> options= obj1.getOptions();
int size=options.size();
obj1.selectByIndex(size-2);


		} catch (NoSuchElementException e) {
			//e.printStackTrace();
			System.out.println("execption occured");	
			throw new RuntimeException();
		}
		finally {
			driver.close();
		}

	}

}
