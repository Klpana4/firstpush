package testcase;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003MergeTestcase extends ProjectMethods {
	@BeforeTest//(dependsOnGroups= {"smoke"})
	
	public void setdata()
	{
		testCaseName="TC003_MergeLEAD";
		testCaseDesc="merge new lead";
		category="smoke";
		author="sethu";
				
		
	}
	@Test//(dependsOnGroups= {"smoke"})

	public  void mergelead() {
		// TODO Auto-generated method stub
		
		WebElement ele9 = locateElement("xpath","//a[text()='Leads']");
		click(ele9);
		WebElement ele10 = locateElement("xpath","//a[text()='Merge Leads']");
		click(ele10);
       WebElement ele11 = locateElement("xpath","(//img[@alt='Lookup'])[1]");
       click(ele11);
        //switching to new window
         switchToWindow(1);
	    Set<String> windowHandles = driver.getWindowHandles();
	    List<String> win= new ArrayList<>();
	    win.addAll(windowHandles);
	   driver.switchTo().window(win.get(1));
	   WebElement ele12 = locateElement("xpath","//input[@name='id']");
	   type(ele12,"12");
		WebElement ele13 = locateElement("xpath","//button[text()='Find Leads']");
        click(ele13);
        WebElement ele14 = locateElement("xpath","(//a[@class='linktext'])[1]");
        click(ele14);
        WebElement ele15 = locateElement("xpath","(//img[@alt='Lookup'])[2]");
        click(ele15);
        switchToWindow(1);
        WebElement ele16 = locateElement("xpath","//input[@name='id']");
 	    type(ele16,"16");
 		WebElement ele17 = locateElement("xpath","//button[text()='Find Leads']");
         click(ele17);
         WebElement ele18 = locateElement("xpath","(//a[@class='linktext'])[1]");
         click(ele18);
          WebElement ele19 = locateElement("xpath","//a[text()='Merge']");
         click(ele19);
         acceptAlert();
         
         
         
         
         
        
	   
	   //driver.findElementByXPath("//input[@name='id']").sendKeys("12");
	   //driver.findElementByXPath("//button[text()='Find Leads']").click();
	  
	  //driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	  
	  //swiching back to primary window
	  //driver.switchTo().window(win.get(0));
	  //driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
	 // windowHandles=driver.getWindowHandles();
	 // win= new ArrayList<>();
	 // win.addAll(windowHandles);
	 // driver.switchTo().window(win.get(1));
	 // driver.findElementByXPath("//input[@name='id']").sendKeys("12");
	  //driver.findElementByXPath("//button[text()='Find Leads']").click();
	 // driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	  //driver.switchTo().window(win.get(0));
	 // driver.findElementByXPath("//a[text()='Merge']").click();
	  //Alert obj=driver.switchTo().alert();
	  //obj.accept();
	  
	  
			  
	   
	   
	   
	   
	   
	     
	     
	}

}
