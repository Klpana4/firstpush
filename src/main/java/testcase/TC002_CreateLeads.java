package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC002_CreateLeads extends ProjectMethods {
	
	
	@BeforeTest
	
	public void setdata()
	{
		testCaseName="TC002_CREATELEAD";
		testCaseDesc="create a new lead";
		category="smoke";
		author="sethu";
		excelfilename = "cl";
				
		
	}
	
	@Test(dataProvider="fetchdata")
	public void cl() {
		
		WebElement eleLogin = locateElement("linktext", "CRM/SFA");
		click(eleLogin);
		WebElement eleLogin1 = locateElement("partiallinktext", "Create Lead");
		click(eleLogin1);
	   WebElement eleLogin2=locateElement("id", "createLeadForm_companyName");
	   type(eleLogin2,"cOGNIZANT");
	   WebElement eleLogin3=locateElement("id", "createLeadForm_firstName");
	   type(eleLogin3,"KALPANA");
	   WebElement eleLogin4=locateElement("id", "createLeadForm_lastName");
	   type(eleLogin4,"KALPANA");
	   WebElement eleLogin5=locateElement("id", "createLeadForm_dataSourceId");
	   selectDropDownUsingText(eleLogin5,"Direct Mail");
	   WebElement eleLogin6=locateElement("class", "smallSubmit");
	   click(eleLogin6);
	   
	   
	   
	   
	   closeBrowser();
}
	
	


	


	
	
	
}


